

'''
	python3 status.proc.py "activities/tar/_status/drive_dir_to_memory_tar/status_1.py"
'''


import shutil
from os.path import dirname, join, normpath
import pathlib
import sys
import zipfile

def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/parties'
])

#/
#
from cryptography.fernet import Fernet
#
#
from ships.paths.directory.tar.drive_directory_to_memory_tar import drive_directory_to_memory_tar	
from ships.paths.directory.tar.memory_tar_to_drive_directory import memory_tar_to_drive_directory	
#
from movies_ogv.activities.FS.file.delete_abandon import delete_abandon_file	
from movies_ogv.activities.FS.directory.delete_abandon import delete_abandon_directory
#
#
import ships.paths.directory.check_equality as check_equality
#
#
from os.path import dirname, join, normpath
import pathlib
import sys
#
#\

this_folder = pathlib.Path (__file__).parent.resolve ()
original_directory_path = str (normpath (join (this_folder, "constants/directory_1")))

tar_path_without_extension = str (normpath (join (this_folder, "variance/directory_1")))
tar_path = str (normpath (join (this_folder, "variance/directory_1.tar")))

reversed_directory_path = str (normpath (join (this_folder, "variance/directory_1")))

def abandon_file_attempt (file_path):
	try:
		delete_abandon_file (file_path)
	except Exception:
		pass;

def abandon_directory_attempt (directory_path):
	try:
		delete_abandon_directory (directory_path)
	except Exception:
		pass;

abandon_file_attempt (tar_path)
abandon_directory_attempt (reversed_directory_path)

tar_stream = drive_directory_to_memory_tar ({
	"directory_path": original_directory_path
})
memory_tar_to_drive_directory ({
	"tar_stream": tar_stream,
	"directory_path": reversed_directory_path
})

report = check_equality.start (
	original_directory_path,
	reversed_directory_path
)	
assert (
	report ==
	{'1': {}, '2': {}}
), report

print ('equality ensured')

#delete_abandon_file (tar_path)


