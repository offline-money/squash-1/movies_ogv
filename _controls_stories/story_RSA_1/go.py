




'''
	python3 status.proc.py "activities/tar/_status/drive_dir_to_memory_tar/status_1.py"
'''


import shutil
from os.path import dirname, join, normpath
import pathlib
import sys
import zipfile

def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/parties'
])

#/
#
from cryptography.fernet import Fernet
import rsa
#
#
from movies_ogv.activities.RSA.keys_create import create_RSA_keys
from movies_ogv.activities.RSA.key_scan_public import scan_RSA_public_key
from movies_ogv.activities.RSA.key_scan_private import scan_RSA_private_key
#
from movies_ogv.activities.FS.file.delete_abandon import delete_abandon_file	
from movies_ogv.activities.FS.directory.delete_abandon import delete_abandon_directory
#
#
import ships.paths.directory.check_equality as check_equality
#
#
from os.path import dirname, join, normpath
import pathlib
import sys
#
#\

this_folder = pathlib.Path (__file__).parent.resolve ()
RSA_private_key_path = str (normpath (join (this_folder, "variance/RSA.private_key.JSON")))
RSA_public_key_path = str (normpath (join (this_folder, "variance/RSA.public_key.JSON")))

gains = create_RSA_keys ({
	"key_size": 2048,
	
	"write_keys": "yes",
	"write_keys_to_paths": {
		"RSA_private_key": RSA_private_key_path,
		"RSA_public_key": RSA_public_key_path
	}
});

RSA_public_key = scan_RSA_public_key (RSA_public_key_path);
RSA_private_key = scan_RSA_private_key (RSA_private_key_path);

#print ("RSA_public_key:", RSA_public_key)
#print ("RSA_private_key:", RSA_private_key)

notes = b'notes'

notes_encrypted = rsa.encrypt (notes, RSA_public_key)

#print ('notes_encrypted:', notes_encrypted.hex ())

notes_decrypted = rsa.decrypt (notes_encrypted, RSA_private_key)

assert (notes == notes_decrypted)
