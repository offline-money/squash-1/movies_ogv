
import shutil
from os.path import dirname, join, normpath
import pathlib
import sys
import zipfile

def add_paths_to_system (paths):
	
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/parties'
])

#/
#
from cryptography.fernet import Fernet
#
#
from movies_ogv.activities.fernet_1.crypt_encrypt import fernet_1_crypt_encrypt
from movies_ogv.activities.fernet_1.crypt_decrypt import fernet_1_crypt_decrypt
from movies_ogv.activities.fernet_1.key_produce import produce_fernet_1_key
from movies_ogv.activities.fernet_1.key_scan import scan_fernet_1_key
from movies_ogv.activities.file.scan import scan_file
#
#
from os.path import dirname, join, normpath
import pathlib
import sys
#
#\

this_folder = pathlib.Path (__file__).parent.resolve ()
original_file = str (normpath (join (this_folder, "constants/strand.HTML")))
encrypted_file = str (normpath (join (this_folder, "variance/strand.HTML.fernet_1")))
decrypted_file = str (normpath (join (this_folder, "variance/strand.HTML")))
fernet_key_path = str (normpath (join (this_folder, "variance/fernet_key.JSON")))



produce_fernet_1_key ({
	"write_outputs": "yes",
	"outputs": {
		"fernet_key_path": fernet_key_path
	}
});

fernet_1_key = scan_fernet_1_key (fernet_key_path);
print ("fernet_1_key:", fernet_1_key);


#fernet_key = read_fernet_key ("");

fernet_1_crypt_encrypt ({
	"fernet_1_key": fernet_1_key,
	
	"from_file": original_file,
	"to_file": encrypted_file
})


fernet_1_crypt_decrypt ({
	"fernet_1_key": fernet_1_key,
	
	"from_file": encrypted_file,
	"to_file": decrypted_file
})


original = scan_file ({
	"path": original_file
})
	
decrypted = scan_file ({
	"path": decrypted_file
})

assert (original == decrypted)